package com.feng.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
public class FeaturnController {
    @RequestMapping("/tesst")
    public void test() {
        System.out.println("新功能编码");
    }

    @RequestMapping("/test1")
    public void test1() {
        System.out.println("新功能编码11111");
    }

    @RequestMapping("/test2")
    public void test2() {
        System.out.println("新功能编码22222");
        System.out.println("二次提交");
        System.out.println("三次提交");
    }
    @RequestMapping("/test3")
    public void test3() {
        System.out.println("新功能开发暂存1111111");
        System.out.println("666666666666");
        System.out.println("777777777777");

        System.out.println("tj22222");
        System.out.println("tj33333");
    }
}
